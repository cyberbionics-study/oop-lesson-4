from decimal import Decimal
import datetime


class CustomError(Exception):
    def __init__(self, expected_values, got):
        self.expected_values = expected_values
        self.got = got
        self.message = f'Expected one of {expected_values}, but got {got}'
        super().__init__(self.message)


class Gym:
    def __init__(self, gym_fee):
        self.gym_fee = gym_fee
        self.sports = {}
        self.trainers = {}
        self.schedules = {}

    def add_sport(self, name, description):
        self.sports[name] = description

    def add_trainer(self, fullname, trainer_fee, sports):
        self.trainers[fullname] = {"fee": trainer_fee, "sports": sports}

    def add_schedule(self, name, days, time, schedule_fee):
        self.schedules[name] = {"days": days, "time": time, "fee": schedule_fee}

    def find_trainer_by_lastname(self, lastname):
        for fullname, info in self.trainers.items():
            if fullname.split()[-1] == lastname:
                return f"Fullname: {fullname}, Fee: {info['fee']}, Sports: {info['sports']}"
        raise CustomError(list(self.trainers.keys()), lastname)

    def form_training(self):
        trainer_name = input('Enter trainer name: ')
        sport_name = input('Enter sport: ')
        schedule_name = input('Enter schedule: ')

        try:
            trainer = next((t for t in self.trainers if t.fullname == trainer_name), None)
            if not trainer:
                raise CustomError([t.fullname for t in self.trainers], trainer_name)
        except CustomError as c:
            print(f'Error: {c}')
            return

        try:
            schedule = next((s for s in self.schedules if s.name == schedule_name), None)
            if not schedule:
                raise CustomError([s.name for s in self.schedules], schedule_name)
        except CustomError as c:
            print(f'Error: {c}')
            return

        try:
            sport = next((s for s in self.sports if s.name == sport_name), None)
            if not sport:
                raise CustomError([s.name for s in self.sports], sport_name)
        except CustomError as c:
            print(f'Error: {c}')
            return

        price = trainer.trainer_fee + schedule.price + self.gym_fee
        result = f"Trainer: {trainer.fullname}, Sport: {sport.name}, Schedule: {schedule.name}, Price: {price}"
        print(result)
        return result


gym = Gym(100)

gym.add_sport("Basketball", "Description of Basketball")
gym.add_sport("Swimming", "Description of Swimming")

gym.add_trainer("John Smith", Decimal('50.00'), ["Basketball", "Swimming"])
gym.add_trainer("Alice Johnson", Decimal('60.00'), ["Swimming"])

gym.add_schedule("Morning Workout", ["Monday", "Wednesday", "Friday"], [datetime.time(9, 0)], Decimal('20.00'))
gym.add_schedule("Evening Workout", ["Tuesday", "Thursday"], [datetime.time(18, 30)], Decimal('25.00'))

gym.form_training()
