from decimal import Decimal
import datetime
from typing import List


class CustomError(Exception):
    def __init__(self, expected_values, got):
        self.expected_values = expected_values
        self.got = got
        self.message = f'Expected one of {expected_values}, but got {got}'
        super().__init__(self.message)


def check_value(expected_values, got):
    """value checking function"""
    if got not in expected_values:
        raise CustomError(expected_values, got)


class Sport:
    def __init__(self, name):
        self.name = name


class Trainer:
    def __init__(self, fullname: str, trainer_fee: Decimal, sports: List[Sport]):
        self.fullname = fullname
        self.trainer_fee = trainer_fee
        self.sports = sports


class Schedule:
    def __init__(self, days: List[datetime.date], time: List[datetime.time], schedule_fee: Decimal):
        self.days = days
        self.time = time
        self.qty_per_week = len(self.days)
        self._schedule_fee = schedule_fee
        self.price = self._schedule_fee * self.qty_per_week


class Gym:
    def __init__(self, gym_fee):
        self.gym_fee = gym_fee
        self.trainers = []
        self.sports = []
        self.schedules = []

    def add_trainer(self, value: Trainer):
        if value in self.trainers:
            return None
        self.trainers.append(value)

    def get_trainers(self):
        return self.trainers

    def add_sport(self, value: Sport):
        if value in self.sports:
            return None
        self.sports.append(value)

    def get_sports(self):
        return self.sports

    def add_schedule(self, value: Schedule):
        if value in self.schedules:
            return None
        self.schedules.append(value)

    def get_schedules(self):
        return self.schedules

    def form_training(self):
        price = 0
        trainer = input('search by trainer info: ')
        sport = input('search sport: ')
        schedule = input('search schedule: ')
        try:
            check_value(self.sports, sport)
        except CustomError as c:
            print(f'Error: {c}')
        try:
            check_value(self.trainers, trainer)
            price += trainer.trainer_fee
        except CustomError as c:
            print(f'Error: {c}')
        try:
            check_value(self.schedules, schedule)
            price += schedule.price
        except CustomError as c:
            print(f'Error: {c}')
        price += self.gym_fee
        result = f"Trainer: {trainer}, Schedule: {schedule}, Price: {price}"
        return result
