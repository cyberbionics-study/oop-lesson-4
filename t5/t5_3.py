from decimal import Decimal
import datetime
from typing import List


class CustomError(Exception):
    """ Custom exception"""
    def __init__(self, expected_values, got):
        self.expected_values = expected_values
        self.got = got
        self.message = f'Expected one of {expected_values}, but got {got}'
        super().__init__(self.message)


class Sport:
    """Sport class for later use and constructions"""
    def __init__(self, name):
        self.name = name


class Trainer:
    """Trainer class. Contains name, fee and list of sports"""
    def __init__(self, fullname: str, trainer_fee: Decimal, sports: List[Sport]):
        self.fullname = fullname
        self.trainer_fee = trainer_fee
        self.sports = sports


class Schedule:
    """Class for training schedules. Contains list of days, list of time and fee"""
    def __init__(self, days: List[datetime.date], time: List[datetime.time], schedule_fee: Decimal):
        self.days = days
        self.time = time
        self.qty_per_week = len(self.days)
        self._schedule_fee = schedule_fee
        self.price = self._schedule_fee * self.qty_per_week


class Gym:
    """Class of sports complex. Contains list of performable sports, available trainers and training schedules, also
    gym's fee. Have function to form your training properties such as trainer, sport and schedule. This function can also
    check if requested property is available at this gym"""
    def __init__(self, gym_fee):
        self.gym_fee = gym_fee
        self.trainers = []
        self.sports = []
        self.schedules = []

    def add_trainer(self, value: Trainer):
        if value in self.trainers:
            return None
        self.trainers.append(value)

    def get_trainers(self):
        return self.trainers

    def add_sport(self, value: Sport):
        if value in self.sports:
            return None
        self.sports.append(value)

    def get_sports(self):
        return self.sports

    def add_schedule(self, value: Schedule):
        if value in self.schedules:
            return None
        self.schedules.append(value)

    def get_schedules(self):
        return self.schedules

    def form_training(self):
        trainer_name = input('Enter trainer name: ')
        sport_name = input('Enter sport: ')
        schedule_name = input('Enter schedule: ')

        try:
            trainer = next((t for t in self.trainers if t.fullname == trainer_name), None)
            if not trainer:
                raise CustomError([t.fullname for t in self.trainers], trainer_name)
        except CustomError as c:
            print(f'Error: {c}')
            return

        try:
            schedule = next((s for s in self.schedules if s.name == schedule_name), None)
            if not schedule:
                raise CustomError([s.name for s in self.schedules], schedule_name)
        except CustomError as c:
            print(f'Error: {c}')
            return

        try:
            sport = next((s for s in self.sports if s.name == sport_name), None)
            if not sport:
                raise CustomError([s.name for s in self.sports], sport_name)
        except CustomError as c:
            print(f'Error: {c}')
            return

        price = trainer.trainer_fee + schedule.price + self.gym_fee
        result = f"Trainer: {trainer.fullname}, Sport: {sport.name}, Schedule: {schedule.name}, Price: {price}"
        print(result)
        return result
    