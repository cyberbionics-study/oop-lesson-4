"""
я трогал пока только 1, такчто любую после: Створіть програму
спортивного комплексу, у якій передбачене меню: 1 - перелік видів спорту, 2 - команда тренерів,
3 - розклад тренувань, 4 - вартість тренування. Дані зберігати у словниках. Також передбачити пошук
по прізвищу тренера, яке вводиться з клавіатури у відповідному пункті меню. Якщо ключ не буде знайдений,
створити відповідний клас-Exception, який буде викликатися в обробнику виключень.

"""


import datetime
from decimal import Decimal
from typing import List


class Sport:
    def __init__(self, name: str):
        self.name = name


class Trainer:
    def __init__(self, fullname: str, sport: List[Sport]) -> None:
        self.fullname = fullname
        self.sport = sport


class Schedule:
    def __init__(
            self,
            start: datetime.datetime,
            duration: datetime.timedelta
    ):
        self.start = start
        self.duration = duration


class Training:
    def __init__(self, sport: Sport, price: Decimal, schedule: Schedule, trainer: Trainer):
        self.sport = sport
        self.price = price
        self.schedule = schedule
        self.trainer = trainer


class Gym:
    def __init__(self):
        self.trainers = []
        self.schedules = []
        self.sports = []

    def add_sports(self, name) -> Sport | None:
        if name in self.sports:
            return None
        sport = Sport(name)
        self.sports.append(sport)
        return sport

    def get_sports(self):
        return self.sports


if __name__ == "_main__":
    ...


