from numbers import Number
from math import sqrt


def get_float(text: str):
    """Check for a real number"""
    try:
        return float(text)
    except ValueError:
        return 'wrong input'


def get_stepen(number_1: Number, number_2: Number) -> None:
    """The function calculates and outputs the power"""
    print(f'{number_1} ** {number_2} = {number_1 ** number_2}')


def get_kub_koren(number_1: Number) -> None:
    """The function calculates and outputs the cube root of a number"""
    if number_1 < 0:
        print(f'Кубічний корень {number_1} = {abs(number_1) ** (1 / 3) * (-1)}')
    else:
        print(f'Кубічний корень {number_1} = {number_1 ** (1 / 3)}')
