def pr_red(skk):
    """ Функція друку червоним кольором """
    print("\033[1;31m{}\033[00m" .format(skk))


def pr_yellow(skk):
    """" Функція друку жовтим кольором """
    print("\033[3;93m   {}\033[00m" .format(skk))


def pr_green(skk):
    """" Функція друку зеленим кольором """
    print("\033[4;92m{}\033[00m" .format(skk))

def pr_blue(skk):
    """" Функція друку синім кольором """
    print("\033[4;34m{}\033[00m".format(skk))
