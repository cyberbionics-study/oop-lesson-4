
def helper():
    return ("\n Available math operations:\n"
            "     '+'     -->    Addition (Finding the Sum);\n"
            "     '-'     -->    Subtraction (Finding the difference);\n"
            "     '*'     -->    Multiplication (Finding the product);\n"
            "     '/'     -->    Division (Finding the quotient);\n"
            " '^' or '**' -->    Exponentiation (Raise to the power);\n"
            "    'sq2'    -->    Finding roots (square root);\n"
            "    'sq3'    -->    Finding roots (cubic root);\n"
            "    'sin'    -->    Finding sinus function;\n"
            )


def cal_sum():
    """Summation function"""
    a = float(input("a= "))
    b = float(input("b= "))
    return 'RESULT:\n{} + {} = {}'.format(a, b, (a + b))


def cal_diff():
    """Difference function"""
    a = float(input("a= "))
    b = float(input("b= "))
    return 'RESULT:\n{} - {} = {}'.format(a, b, (a - b))
