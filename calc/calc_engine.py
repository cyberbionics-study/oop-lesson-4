from RGBPrint import pr_red, pr_yellow, pr_green
import DV_func
import NK_func
import SK_func
from Nick import get_float, get_stepen, get_kub_koren


print("CALCULATOR".center(100, "."))
pr_yellow("Enter <?> or <help> for list of available math operations, <end> to terminate program.")
action: str = ""
while True:
    action = input(">>: ")
    match action:
        case "+": pr_green(DV_func.cal_sum())
        case "-": pr_green(DV_func.cal_diff())
        case "*": pr_green(NK_func.cal_mult())
        case "/": pr_green((SK_func.cal_division()))
        case "^" | "**":
                num1, num2 = get_float(input("Число 1: ")), get_float(input("Число 2: "))
                if num1 != 'wrong input' and num2 != 'wrong input':
                    get_stepen(num1, num2)
        case "sq2": pr_green(NK_func.cal_sq2())
        case "sq3":
                num = get_float(input("Число: "))
                if num != 'wrong input':
                    get_kub_koren(num)
        case "sin": pr_green(SK_func.cal_sin())
        case "?" | "help": pr_yellow(DV_func.helper())
        case "end": exit(code="Thanks for using!".center(100))
        case _:
            pr_red("command unknown")
