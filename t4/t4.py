class CustomError(Exception):
    """My custom error: you cannot input None type"""
    def __init__(self):
        self.message = "Unfortunately you can't input None"
        super().__init__(self.message)


def some_func():
    """Some input function"""
    a = input('>> ')
    if a == 'None':
        raise CustomError()


try:
    some_func()
except CustomError as c:
    print(f'CustomError: {c} \n move forward')

