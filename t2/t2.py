
print("CALCULATOR".center(100, "."))
print("Enter <?> or <help> for list of available math operations, <end> to terminate program.")
action: str = ""


def helper():
    """Help function"""
    return ("\n Available math operations:\n"
            "     '+'     -->    Addition (Finding the Sum);\n"
            "     '-'     -->    Subtraction (Finding the difference);\n"
            "     '*'     -->    Multiplication (Finding the product);\n"
            "     '/'     -->    Division (Finding the quotient);\n"
            " '^' or '**' -->    Exponentiation (Raise to the power);\n"
            )


def cal_sum():
    """Summation function"""
    a = float(input("a= "))
    b = float(input("b= "))
    return 'RESULT:\n{} + {} = {}'.format(a, b, (a + b))


def cal_diff():
    """Difference function"""
    a = float(input("a= "))
    b = float(input("b= "))
    return 'RESULT:\n{} - {} = {}'.format(a, b, (a - b))


def cal_mult():
    """Multiplication function"""
    a = float(input("a= "))
    b = float(input("b= "))
    c = a * b
    return f'RESULT: \n{a} * {b} = {c}'


def cal_division():
    """Division function"""
    a = float(input("a: "))
    b = float(input("b: "))
    try:
        a / b
        return f"RESULT: \n{a} / {b} = {a/b}"
    except ZeroDivisionError as zd:
        print(zd)


def cal_pow():
    """Exponentiation function"""
    a = float(input("input value of base: "))
    b = float(input("input value of power: "))
    try:
        a ** b
        return f"RESULT: \n{a} ^ {b} = {a**b}"
    except ZeroDivisionError as zd:
        print(zd)


class UnexpectedInputError(Exception):
    """Custom exception for unexpected values"""
    def __init__(self, expected_values, got):
        self.expected_values = expected_values
        self.got = got
        self.message = f'Expected one of {expected_values}, but got {got}'
        super().__init__(self.message)


def check_value(expected_values, got):
    """value checking function"""
    if got not in expected_values:
        raise UnexpectedInputError(expected_values, got)


while True:
    """body of the program, where logic is executed"""
    action = input(">>: ")
    try:
        check_value(["+", "-", "*", "/", "^", "**", "?", "help", "end"], action)
        if action == "+":
            print(cal_sum())
        elif action == "-":
            print(cal_diff())
        elif action == "*":
            print(cal_mult())
        elif action == "/":
            print(cal_division())
        elif action in ["^", "**"]:
            print(cal_pow())
        elif action in ["?", "help"]:
            print(helper())
        elif action == "end":
            exit(code="Thanks for using!".center(100))
    except UnexpectedInputError as e:
        print(f"Error: {e}")
