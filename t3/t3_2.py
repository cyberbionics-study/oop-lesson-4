class IsNotStringError(Exception):
    """Exception for not string"""
    def __init__(self):
        self.message = 'String was expected, but got other'
        super().__init__(self.message)


class IsNotIntegerError(Exception):
    """Exception for not integer"""
    def __init__(self):
        self.message = 'Integer was expected, but got other'
        super().__init__(self.message)


class Employee:
    """Class of employee, constructor implements type checking"""
    def __init__(self, name: str, surname: str, department: str, employment_year: int):
        if type(name) == str:
            self.name = name
        else:
            raise IsNotStringError()
        if type(surname) == str:
            self.surname = surname
        else:
            raise IsNotStringError()
        if type(department) == str:
            self.department = department
        else:
            raise IsNotStringError()
        if type(employment_year) == int:
            self.employment_year = employment_year
        else:
            raise IsNotIntegerError()

    def __str__(self):
        return f'\n {self.name} {self.surname}, {self.department}, employed in {self.employment_year}'


class Company:
    """Company class. Company contains list of employees. Also got methods for adding new employees and displaying
    list of people employed after some year"""
    def __init__(self):
        self.employees = []

    def __str__(self):
        employee_str_list = [str(employee) for employee in self.employees]
        return '\n'.join(employee_str_list)

    def add_employee(self, employee: Employee):
        self.employees.append(employee)

    def employed_after(self, value):
        answer = []
        for employee in self.employees:
            if employee.employment_year >= value:
                answer.append(employee)
        return answer


emp1 = Employee('John', 'Doe', 'sales', 2015)
emp7 = Employee('Simon', 'Riley', 'security', 1997)
emp8 = Employee('Susan', 'Miller', 'management', 2021)
some_company = Company()
some_company.add_employee(emp1)
some_company.add_employee(emp7)
some_company.add_employee(emp8)

employees_after_2000 = some_company.employed_after(2000)
for employee in employees_after_2000:
    print(employee)


