class IsNotStringError(Exception):
    def __init__(self):
        self.message = 'String was expected, but got other'
        super().__init__(self.message)


class IsNotIntegerError(Exception):
    def __init__(self):
        self.message = 'Integer was expected, but got other'
        super().__init__(self.message)


class Employee:
    def __init__(self, name: str, surname: str, department: str, employment_year: int):
        if type(name) == str:
            self.name = name
        else:
            raise IsNotStringError()
        if type(surname) == str:
            self.surname = surname
        else:
            raise IsNotStringError()
        if type(department) == str:
            self.department = department
        else:
            raise IsNotStringError()
        if type(employment_year) == int:
            self.employment_year = employment_year
        else:
            raise IsNotIntegerError()

    def __str__(self):
        return f'\n {self.name} {self.surname}, {self.department}, employed in {self.employment_year}'

    # @property
    # def employment_year(self):
    #     return self.employment_year


class Company:
    def __init__(self):
        self.employees = []

    def __str__(self):
        employee_str_list = [str(employee) for employee in self.employees]
        return '\n'.join(employee_str_list)

    def add_employee(self, employee: Employee):
        self.employees.append(employee)

    def employed_after(self, value):
        answer = []
        for employee in self.employees:
            if employee.employment_year >= value:
                answer.append(employee)
        return answer


emp1 = Employee('John', 'Doe', 'sales', 2015)
# emp2 = Employee('', '', '', 0)
# emp3 = Employee('asdasd', 'ы', None, 2015)
# emp4 = Employee(12, '', '', 2015)
# emp5 = Employee('', False, '', 2015)
# emp6 = Employee('', '', '', '2015')
emp7 = Employee('Simon', 'Riley', 'security', 1997)
emp8 = Employee('Susan', 'Miller', 'management', 2021)
some_company = Company()
some_company.add_employee(emp1)
# some_company.add_employee(emp2)
# some_company.add_employee(emp3)
# some_company.add_employee(emp4)
# some_company.add_employee(emp5)
# some_company.add_employee(emp6)
some_company.add_employee(emp7)
some_company.add_employee(emp8)

print(some_company.employed_after(2000))
