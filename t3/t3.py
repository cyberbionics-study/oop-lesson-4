

class Employee:
    def __init__(self, name: str, surname: str, department: str, employment_year: int):
        if isinstance(name, str):
            self.name = name
        else:
            raise
        self.surname = surname
        self. department = department
        self.employment_year = employment_year


class Company:
    def __init__(self):
        self.employees = []

    def add_employee(self, employee: Employee):
        self.employees.append(employee)


